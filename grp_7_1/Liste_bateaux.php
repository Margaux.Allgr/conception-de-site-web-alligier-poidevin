<!DOCTYPE html>
<?php session_start()?>
<html>
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
    </head>

    <body style="min-height: 100%; margin: 0; padding: 0; position: relative; background-color: #daeef0 ">
        <?php include "header.inc.php"?>

        <h1 class="text-center">Les bateaux présents :</h1><br/><br/><br/>
        

        <div class="container">
            <div class="row">
                <div class="col-md-0"></div>

                <div class="col-md-12">
                <?php

                $mysqli= mysqli_connect("localhost","grp_7_1","aiThooM5du","bdd_7_1");
				$req_pre = mysqli_prepare($mysqli, 'SELECT `matricule`,`nom`,`lienphoto` FROM `bateau`');
				mysqli_stmt_execute($req_pre);
				mysqli_stmt_bind_result($req_pre,$matricule,$nom,$lienphoto);
				while (mysqli_stmt_fetch($req_pre)){?>
                <div class="text-center"><img href='Bateau.php?matricule=<?php echo $matricule?>' title='<?php echo $nom?>' src="photosBateaux/<?php echo $lienphoto ?>"max-width: 100px; height: auto;></div>
                <br/>
                
                 <a href='Bateau.php?matricule=<?php echo $matricule?>' title=""><div class="text-center"><?php echo $nom?></div></a><br/><br/>
				<?php
				}
				mysqli_close($mysqli);
				?>
                
               


                </div>

                <div class="col-md-0"></div>
            </div>    
        </div>



<?php include "footer.inc.php" ?>   
    </body>
</html>