<?php
session_start();
$nom=$_POST['nom'];
$prenom=$_POST['prenom'];
$email=$_POST['email'];
$mdp=$_POST['mdp'];

require_once('param.inc.php');
$mysqli=new mysqli($host, $login, $password, $dbname);

if(!($stmt=$mysqli -> prepare("INSERT INTO `utilisateur`(`nom`, `prenom`, `email`, `mdp`) VALUES(?,?,?,?)")))
{
    $_SESSION['erreur']= "Erreur";
    }else {   
        $mdpcrypte=password_hash($mdp, PASSWORD_DEFAULT );
        $stmt->bind_param('ssss',$nom,$prenom,$email,$mdpcrypte); 
        if (!$stmt->execute()){
            $_SESSION['erreur']= "Erreur"; 
        }else{
            header("Location: Connexion.php");
        }        
    }

?>