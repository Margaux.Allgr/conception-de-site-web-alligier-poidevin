<?php 
session_start(); ?>

<!DOCTYPE html>
<html style="height: 100%">
    <head>
        <meta charset="utf-8" />
        <title>Armada</title>
        <!--Custom style for this template-->
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
    </head>


    <body style="min-height: 100%; margin: 0; padding: 0; position: relative;background-color: #daeef0">
        
        
        


             <?php if(empty($_SESSION)){ ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert"> Pour avoir accès à toutes les fonctionnalités <a href="Connexion.php">connectez-vous</a>.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                <?php }

            else {
                    if (isset($_SESSION['message'])){ 
                   echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">'.$_SESSION['message'].'
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>';
                  
                 unset($_SESSION['message']);
                    }
                 } ?>


         <?php include "header.inc.php" ?>


        <div class="container" style="background-color: #daeef0">
              
            

            <div class="row">
                <div class="col-4">
                    <div class="row" style="margin :10px"> <u>Présentation du site :</u> <br><br>
                	En 2019, l’association de L’Armada de la liberté fêtera ses 30 ans d’existence et sa 7e édition, qui aura lieu du 6 au 16 juin 2019. <br> 
                	Des millions de visiteurs sont de nouveau attendus sur les quais du port de Rouen. Ils profiteront de concerts,
                   de feux d’artifices géants et de nombreuses animations et bien sûr visiteront gratuitement la cinquantaine de navires présents. Les plus beaux et les plus grands voiliers, 
                   les bâtiments militaires les plus modernes et d’autres bateaux d’exception venus du monde entier auront remonté la Seine sur 120 kilomètres à travers les magnifiques paysages de la Normandie. <br> 
                	Du Havre jusqu’à Paris, l’Armada est devenue l’événement fédérateur de l’axe Seine. 
                    </div>
                    <div class="row"> Programme : <br> blah <br> blah <br> blah <br>blah
                    </div>
                </div>
                <div class="col-8"> <p> illustration de bateaux </p>
                <!-- Carrousel -->
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                     <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                     </ol>
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="images/Voilier1.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" src="images/Voilier1.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                           <img class="d-block w-100" src="images/Voilier1.jpg" alt="Third slide">
                        </div>
                     </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                     </a>
                </div>
                
                </div>
            </div>

         
        </div>
        <?php include "footer.inc.php" ?>
    </body>

</html>