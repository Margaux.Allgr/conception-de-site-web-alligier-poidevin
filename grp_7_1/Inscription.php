<!DOCTYPE html>
<?php session_start()?>
<html>
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
    </head>
    <body style="min-height: 100%; margin: 0; padding: 0; position: relative; background-color: #daeef0 ">
        <?php include "header.inc.php" ?>
        <title>Inscription</title>
        

        <div class="text-center"><h1>Inscription </h1></div><br>

        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">         
                    <form class="form-signin" method="post" action="inscriptionFormulaire.php">

                        <label for="inputNom">Nom :</label>
                        <label for="inputNom" class="sr-only">Nom</label>
                        <input type="nom" id="inputNom" class="form-control" placeholder="Nom" required="" autofocus="" name="nom"><br>

                        <label for="inputPrenom">Prénom :</label>
                        <label for="inputPrenom" class="sr-only">Prenom</label>
                        <input type="prenom" id="inputPrenom" class="form-control" placeholder="Prénom" required="" name="prenom"><br>

                        <label for="inputEmail">Adresse e-mail :</label>
                        <label for="inputEmail" class="sr-only">Adresse e-mail</label>
                        <input type="email" id="inputEmail" class="form-control" placeholder="Adresse e-mail" required=""name="email" ><br>
           
                        <label for="inputPassword">Mot de passe :</label>
                        <label for="inputPassword" class="sr-only">Mot de passe</label>
                        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required="" name="mdp"><br>

                        
           
                        <div class="text-center"><button class="btn btn-md btn-info" type="submit">S'enregistrer</button></div><br>
                        </div>
                    </form>
                    
                     
                <div class="col-md-4"></div>
            </div>
         
        </div>
        <?php include "footer.inc.php" ?>   
    </body>
</html>