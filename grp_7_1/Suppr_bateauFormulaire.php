<?php
session_start();

$matricule=$_GET['imat'];

require_once('param.inc.php');
$mysqli=new mysqli($host, $login, $password, $dbname);

if(!($stmt=$mysqli -> prepare("DELETE FROM `bateau` WHERE `matricule`=?")))
{
    $_SESSION['erreur']= "Erreur";
    }else {   
        $stmt->bind_param('s',$matricule); 
        if (!$stmt->execute()){
            $_SESSION['erreur']= "Erreur"; 
        }else{
            header("Location: Liste_bateaux.php");
        }        
    }
        
?>
