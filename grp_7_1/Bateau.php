<?php 
session_start();
$imat= $_GET['matricule'];
?>

<!DOCTYPE html>
<html style="height: 100%">
    <head>
        <meta charset="utf-8" />
        <title>Armada</title>
        <!--Custom style for this template-->
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
    </head>


    <body style="min-height: 100%; margin: 0; padding: 0; position: relative;background-color: #daeef0">  
       
     
        <?php include "header.inc.php";

        $mysqli= mysqli_connect("localhost","grp_7_1","aiThooM5du","bdd_7_1");
        $req_pre = mysqli_prepare($mysqli,'SELECT * FROM `bateau` WHERE `matricule` =?');
        mysqli_stmt_bind_param($req_pre, "s", $imat);
        mysqli_stmt_execute($req_pre);
        mysqli_stmt_bind_result($req_pre,$matricule,$nom,$nationnalite,$datearrivee,$datedepart,$lienpdf,$lienphoto,$mailresponsable,$description);
        mysqli_stmt_fetch($req_pre);
        mysqli_close($mysqli);


        if ($nom!= ""){ ?>
     
          <br><br>
         <div class="text-center"><h1><?php echo $nom ?></h1></div>
         <br>
         <div class="text-center"><img src="photosBateaux/<?php echo $lienphoto ?>"max-width: 100px; height: auto;></div>
         <br/>

         <!-- Afficher les infos du bateau : matricule, nationnalite, date arrivee, date depart, lien pdf,
         mail responsable, description -->
          
          <div class="row">
	            <div class="col-sm-6 text-center" >
                  Imatriculation : <?php echo $matricule ?><br/><br/>
                  Nationnalité :  <?php echo $nationnalite ?><br/><br/>
		          Adresse mail du responsable : <?php echo $mailresponsable ?><br/><br/>
                </div>
                <div class="col-sm-6 text-center">
                  date d'arrivée : <?php echo $datearrivee ?><br/><br/>
                  date de départ : <?php echo $datedepart ?><br/><br/>
                  Description : <?php echo $description ?><br/><br/>
                </div>
           </div>


             <!-- téléchargement pdf -->
             <?php if (isset( $_SESSION['statut'])){ ?>
                <br/><div class="text-center"><input class="btn btn-md btn-info" type='button' onclick="window.open('pdfBateaux/<?php echo $lienpdf ?>');" value="Telecharger le PDF" /></div><br/><br/>
             <?php }else{?>
            <!-- message erreur téléchargement pdf -->
               <div class="text-center"> <p> Pour télécharger le pdf de description du bateau, <a href="Connexion.php">connectez-vous</a> </p>
            </div>
            <?php }?>

		<?php }else{ ?>
                <br/>
             <div id="intertitre">URL invalide </div> <?php 
             
        }?>


        <?php include "footer.inc.php" ?>

    </body>
</html>