<!DOCTYPE html>
<?php session_start()?>
<html>
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
    </head>
    <body style="min-height: 100%; margin: 0; padding: 0; position: relative; background-color: #daeef0 ">

        <?php ///include "param.inc.php" ?>
        <?php include "header.inc.php"?>
        <title>Identification</title>
        <div class="text-center"><h1>Connexion</h1></div><br>

        <div class="container">
            <div class="row">
                <div class="col-md-4"> </div>
                <div class="col-md-4">         
                <?php if (isset($_SESSION['erreur'])){
                    echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">'.$_SESSION['erreur'].'
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>';
                  unset($_SESSION['erreur']);
                    } ?>
                    <form class="form-signin" method="post" action="connexionFormulaire.php">
                        <label for="inputEmail" class="sr-only">Adresse e-mail</label>
                        <input type="email" id="inputEmail" class="form-control" placeholder="Adresse e-mail" required autofocus name="email">
                        <label for="inputPassword" class="sr-only">Mot de passe</label>
                        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required name="mdp"><br>
                        <div class="text-center"><button class="btn btn-md btn-info" type="submit">Se connecter</button></div><br>
                     </form> 

                      <form method="get" action="Inscription.php"><p class="text-right">Pas encore inscrit? <button class="btn btn-sm btn-outline-info" type="submit">Créer un compte</button></form></p>
                    </div>
                     
                <div class="col-md-4"></div>
            </div>
        </div>
    <?php include "footer.inc.php" ?>     
    </body>
</html>