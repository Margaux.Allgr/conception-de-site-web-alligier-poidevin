<?php 
session_start();
?>

<!DOCTYPE html>
<html style="height: 100%">
    <head>
        <meta charset="utf-8" />
        <title>Armada</title>
        <!--Custom style for this template-->
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
    </head>


    <body style="min-height: 100%; margin: 0; padding: 0; position: relative;background-color: #daeef0">  
       
        <?php include "header.inc.php" ?>

        <?php if (isset( $_SESSION['statut'])&&( $_SESSION['statut'])>0){ ?>

         <title>Création page bateau</title>
         <div class="text-center"><h1>Création d'une page de bateau :</h1></div><br>



            <div class="container">
                <div class="row">
                    <div class="col-md-4"> </div>
                    <div class="col-md-4">         
                        <form class="form-signin" method="post" action="Creation_bateauFormulaire.php">
                    

                         <label for="inputMatricule">Matricule:</label>
                         <label for="inputMatricule" class="sr-only">Matricule</label>
                         <input type="matricule" id="inputMatricule" class="form-control" placeholder="Matricule" required="" autofocus="" name="matricule"><br>
                        
                         <label for="inputNom">Nom du bateau:</label>
                         <label for="inputNom" class="sr-only">Nom du bateau</label>
                         <input type="nom" id="inputNom" class="form-control" placeholder="Nom du bateau" required="" autofocus="" name="nomBateau"><br>

                         <label for="inputNationalite">Nationalité:</label>
                         <label for="inputNationalite" class="sr-only">Netionalité</label>
                         <input type="nationalite" id="inputNationalite" class="form-control" placeholder="Nationalité" required="" name="nationalite"><br>

                         <label for="inputDateArrivee">Date d'arrivée:</label><br>
                         <label for="inputDateArrivee" class="sr-only">Date d'arrivée</label>
                         <input type="date" name="DateArrivee" ><br><br>
           
                         <label for="inputDateDepart">Date de départ:</label><br>
                         <label for="inputDateDepart" class="sr-only">Date de départ</label>
                         <input type="date" name="DateDepart"><br><br>

                         <label for="Pdf">Pdf de description: </label>
                         <input type="file" class="form-control-file" id="Pdf" name="pdf"><br>

                         <label for="Photo">Photo du bateau: </label>
                         <input type="file" class="form-control-file" id="Photo" name="photo"><br>      

                         <label for="Photo">Rapide description: </label><br>
                         <textarea name="textarea" rows="10" cols="50" placeholder="Entrez une rapide description" required=""></textarea><br><br>                                              

           
                            <div class="text-center"><button class="btn btn-md btn-info" type="submit">Créer la page</button></div><br>
                            </div>
                        </form>

                    </div>
                    <div class="col-md-4"></div>

                </div>

            </div>

        <?php }else{?>
            <!-- message erreur acces -->
                <p>Erreur: vous n'avez pas les droits d'accès à cette page, connectez vous en tant que propriétaire ou administrateur. </p>

        <?php }?>

        <?php include "footer.inc.php" ?>
    </body>

</html>