
<div class="row" style="height:200px;background:transparent url('images/armada.jpg') no-repeat center left; background-color: #daeef0; margin: 0; padding: 0; border: 0; vertical-align: middle;">
    <div class="col-md-6" text-left><img src="images/armada_titre.png" > </div>
    <div class="col-md-6 text-right"><img src="images/Vignette_ronde.png" height="150" width="150"  ></div>
</div>

<div>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #3baeb8;height:65px;">
    <!-- Bouton Accueil -->
    <a class="navbar-brand" href="index.php" >Accueil</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Remplissage de la navbar si personne n'est connecté-->
    <?php if(empty($_SESSION)) { ?>
        
        <div class="collapse navbar-collapse" id="navbarNav" style="z-index:10;">
        <ul class="navbar-nav" style="background-color: #66c9d2">

        <li class="nav-item">
            <a class="nav-link" href="Liste_bateaux.php" style="margin :10px">Liste des bateaux</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Modif_roles.php" style="margin :10px">Modifier les roles</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Connexion.php" style="margin :10px">Se connecter </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Inscription.php" style="margin :10px">S'inscrire</a>
          </li>
          
        </ul>
      </div>
    
    <?php } else {
      
      
      if( $_SESSION['statut']==2){?>  <!-- Remplissage de la navbar si un admin 2 est connecté --> 
    

     
        <div class="collapse navbar-collapse" id="navbarNav" style="position:relative;z-index:10;">
        <ul class="navbar-nav" style="background-color: #66c9d2 ">

        <li class="nav-item">
            <a class="nav-link" href="Liste_bateaux.php" style="margin :10px">Liste des bateaux</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Modif_roles.php" style="margin :10px">Modifier les roles</a>
          </li>          
          <li class="nav-item">
            <a class="nav-link" href="Deconnexion.php" style="margin :10px">Déconnexion</a>
          </li>
          
        </ul>
      </div>

      <?php } 
      
      elseif ( $_SESSION['statut']==1) {?>
    

    <!-- Remplissage de la navbar si un propriétaire 1 est connecté -->
    <div class="collapse navbar-collapse" id="navbarNav" style="position:relative;z-index:10;">
        <ul class="navbar-nav" style="background-color: #66c9d2 ">

        <li class="nav-item">
            <a class="nav-link" href="Liste_bateaux.php" style="margin :10px">Liste des bateaux</a>
          </li>
          <!-- Mettre une condiion si le proprio peut créer une seule page bateau-->
          <li class="nav-item">
            <a class="nav-link" href="Creation_bateau.php" style="margin :10px">Création page bateau</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Deconnexion.php" style="margin :10px">Déconnexion</a>
          </li>
          
        </ul>
      </div>


    
      <?php } 
      
      else {?> <!-- Remplissage de la navbar si un utilisateur 0 est connecté -->  

    <div class="collapse navbar-collapse" id="navbarNav" style="position:relative;z-index:10;">
        <ul class="navbar-nav" style="background-color: #66c9d2 ">

        <li class="nav-item">
            <a class="nav-link" href="Liste_bateaux.php" style="margin :10px">Liste des bateaux</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Deconnexion.php" style="margin :10px">Déconnexion</a>
          </li>
          
        </ul>
      </div>

    <?php }
  }?>    


</nav>
</div>

