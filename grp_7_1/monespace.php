<!DOCTYPE html>
<?php session_start()?>
<html>

<head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
</head>

<body style="min-height: 100%; margin: 0; padding: 0; position: relative; background-color: #daeef0 ">
    <?php include "header.inc.php"; 
        require_once('param.inc.php');
        $bdd=new mysqli($host, $login, $password, $dbname);
        
        ?>
    <title>Mon espace</title>

    <div class="text-center"><h1><br>Mon espace</h1></div><br>

    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">

            <div class="text-left"><h2> Mes informations :</h2></div><br>

            <form>
            <div class="form-group">
                <label for="inputNom">Nom :</label>
                <input type="nom" id="inputNom" class="form-control" placeholder="<?php echo $_SESSION['nom'];?>">
            </div>
            <div class="form-group">
                <label for="inputPrenom">Prenom :</label>
                <input type="prenom" id="inputPrenom" class="form-control" placeholder="<?php echo $_SESSION['prenom'];?>">
            </div>
            <div class="form-group">
                <label>Rôle :</label>
                <?php if( $_SESSION['statut']==0){ 
                    echo 'Visiteur';
                } else if( $_SESSION['statut']==1){ 
                    echo 'Propriétaire';
                } else if( $_SESSION['statut']==2){ 
                    echo 'Administrateur';
                }
                ?>
            </div>

            
            </form>
            
            <button class="btn btn-info" type="submit">Enregistrer</button><br><br>


            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

<?php include "footer.inc.php" ?>
</body>
</html>