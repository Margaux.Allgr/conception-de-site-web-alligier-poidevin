<div style="height:250px;background-color: #ffffff ;margin: 0; padding: 0; border: 0;  left:0; right:0; display : table-row;" class="row">

    <div class="row" style="margin:0px; padding:0;">   
        <div class="col-md-12" style="margin:0px; padding:0;">
            <p style="margin: 20px; text-align:center">
                 <a style="margin: 20px" href="https://www.normandie.fr/" title="https://www.normandie.fr/"><img src="images/normandie.png"></a>
                 <a style="margin: 20px" href="https://www.seinemaritime.fr/" title="https://www.seinemaritime.fr/"><img src="images/76.png"></a>
                 <a style="margin: 20px" href="http://www.metropole-rouen-normandie.fr/" title="http://www.metropole-rouen-normandie.fr/"><img src="images/metropole.png"></a>
                 <a style="margin: 20px" href="http://www.haropaports.com/" title="http://www.haropaports.com/"><img src="images/haropaport.png"></a>
                 <a style="margin: 20px" href="http://www.armada.org/" title="http://www.armada.org/"><img src="images/cci.png"></a>
                 <a style="margin: 20px" href="http://www.rouen.fr/" title="http://www.rouen.fr/"><img src="images/rouen.png"></a>
                 <a style="margin: 20px" href="http://www.defense.gouv.fr/marine" title="http://www.defense.gouv.fr/marine"><img src="images/marine.png"></a>
            </p>
         </div>
    </div>

    <div class="row" style="margin:0px; padding:0;">
      <div class="col-md-12" style="margin:0px; padding:0;">
            <p style="text-align:center">    
                2018 © Armada - site réalisé par Margaux ALLIGIER et Marine POIDEVIN
        
    
             </p>
        </div>
    </div>

</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>