<!DOCTYPE html>
<?php session_start()?>
<html>

<head>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
</head>

<body style="min-height: 100%; margin: 0; padding: 0; position: relative; background-color: #daeef0 ">
    <?php include "header.inc.php"; 
if (isset( $_SESSION['statut'])&&( $_SESSION['statut'])==2){    
       
       require_once('param.inc.php');
        $bdd=new mysqli($host, $login, $password, $dbname);
        
        ?>
    <title>Modifier les rôles</title>

    <div class="text-center"><h1><br>Modification de rôles</h1></div><br>

    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-5">

                <p> Selectionnez une personne dont le rôle doit changer :</p>

                <form class="form-signin" method="POST" action="visiteur.php">

                    <select name="email">
                        <?php 
                            $reponse=$bdd->query("SELECT `nom`, `prenom`, `statut`, `email` FROM `utilisateur`");
                            if ($reponse->num_rows==0){
                                echo '<option>Aucun résultat</option>';
                            }else{
                            while($tuple=$reponse->fetch_assoc()){
                                $nom=htmlentities($tuple['nom']);
                                $prenom=htmlentities($tuple['prenom']);
                                $email=htmlentities($tuple['email']);
                                $statut=htmlentities($tuple['statut']);
                                echo '<option value="'.$email.'">'.$nom.' '.$prenom.' '.$email.'   rôle : '.$statut.'</option>';
                            }
                        }    
                    ?>

                    </select>
                    <div>Nouveau poste : </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="poste" id="exampleRadios1" value="0"
                            checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Visiteur
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="poste" id="exampleRadios1" value="1">
                        <label class="form-check-label" for="exampleRadios1">
                            Propriétaire
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="poste" id="exampleRadios2" value="2">
                        <label class="form-check-label" for="exampleRadios2">
                            Administrateur
                        </label>
                    </div>
                    <br>

                <button class="btn btn-info" type="submit">OK</button>



            </div>

            <div class="col-md-4"></div>

        </div>
    </div>
    <?php }else{?>
            <!-- message erreur acces -->
                <p>Erreur: vous n'avez pas les droits d'accès à cette page, connectez vous en tant qu'administrateur. </p>

        <?php }?>

    <?php include "footer.inc.php" ?>

</body>

</html>