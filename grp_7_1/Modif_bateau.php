<?php 
session_start();
$imat= $_GET['matricule'];
$matricule=$imat;
?>

<!DOCTYPE html>
<html style="height: 100%">
    <head>
        <meta charset="utf-8" />
        <title>Armada</title>
        <!--Custom style for this template-->
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="shortcut icon" type="image/x-icon" href="images/Vignette_ronde.png" />
    </head>


    <body style="min-height: 100%; margin: 0; padding: 0; position: relative;background-color: #daeef0">  
       
     
        <?php include "header.inc.php"; 

        $mysqli= mysqli_connect("localhost","grp_7_1","aiThooM5du","bdd_7_1");
        $req_pre = mysqli_prepare($mysqli,'SELECT * FROM `bateau` WHERE `matricule` =?');
        mysqli_stmt_bind_param($req_pre, "s", $imat);
        mysqli_stmt_execute($req_pre);
        mysqli_stmt_bind_result($req_pre,$matricule,$nom,$nationnalite,$datearrivee,$datedepart,$lienpdf,$lienphoto,$mailresponsable,$description);
        mysqli_stmt_fetch($req_pre);
        mysqli_close($mysqli);
        ?>

        <?php if( ($_SESSION['statut']==2) || ( ($_SESSION['statut']==1)&& ($_SESSION['email']==$mailresponsable)) ){ ?>

         <div class="text-center"><h1>Modifier page du bateau matricule <?php echo $matricule ?> :</h1></div><br>



            <div class="container">
                <div class="row">
                    <div class="col-md-4"> </div>
                    <div class="col-md-4">         
                        <form class="form-signin" method="post" action="Modif_bateauFormulaire.php?matricule=<?php echo $matricule?>" enctype="multipart/form-data">
                    
                         <label for="inputNom">Nom du bateau:</label>
                         <label for="inputNom" class="sr-only">Nom du bateau</label>
                         <input type="nom" id="inputNom" class="form-control"  value="<?php echo $nom ?>" required="" autofocus="" name="nomBateauModif"><br>

                         <label for="inputNationalite">Nationalité:</label>
                         <label for="inputNationalite" class="sr-only">Netionalité</label>
                         <input type="nationalite" id="inputNationalite" class="form-control"  value="<?php echo $nationnalite ?>" required="" name="nationaliteModif"><br>

                         <label for="inputDateArrivee">Date d'arrivée:</label><br>
                         <label for="inputDateArrivee" class="sr-only">Date d'arrivée</label>
                         <input type="date" name="DateArriveeModif"  value=<?php echo $datearrivee ?>><br><br>
           
                         <label for="inputDateDepart">Date de départ:</label><br>
                         <label for="inputDateDepart" class="sr-only">Date de départ</label>
                         <input type="date" name="DateDepartModif" value=<?php echo $datedepart ?>><br><br>

                         <label for="Pdf">Pdf de description: </label>
                         <input type="hidden" name="maxPDF" value="999999999" >
                         <input type="file" class="form-control-file" id="pdf" name="pdfModif"><br>
                        

                         <label for="Photo">Photo du bateau: </label>
                         <input type="hidden" name="maxPHOTO" value="999999999" >
                         <input type="file" class="form-control-file" id="photo" name="photoModif"><br>      

                         <!-- Si le créateur de la page est admin rajouter une possibilité de modifier le proprio du bateau ?-->
                         <?php if($_SESSION['statut']==2){ ?>
                        
                        
                         <?php }?>

                         <label for="Photo">Rapide description: </label><br>
                         <textarea name="textareaDescriptionModif" rows="10" cols="50"  required=""><?php echo $description ?></textarea><br><br>                                              
           
                            <div class="text-center"><button class="btn btn-md btn-info" type="submit">Modifier la page</button></div><br>
                            
                        </form>

                    </div>
                    <div class="col-md-4"></div>
                </div>
                
                <div class="row">
                 <div class="col-md-12">
                         
                        <div class="text-center"><button class="btn btn-md btn-info" href='Suppr_bateauFormulaire.php?matricule=<?php echo $matricule?>' type="submit">Supprimer la page</button></div><br>
                        
                 </div>
                </div>    
            </div>




        <?php } else {?>
            <br>
            <br>
            <div class="text-center"><h2> Vous ne pouvez pas accéder à cette page !</h2></div>
            <br>
            <br>
        <?php } ?>

        <?php include "footer.inc.php" ?>

</body>
</html>